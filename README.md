<h1 align="center">Welcome to the Pajamas Adoption Scanner 👋</h1>
<p>
  <a href="https://gitlab.com/gitlab-org/frontend/pajamas-adoption-scanner" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://gitlab.com/gitlab-org/frontend/pajamas-adoption-scanner/-/blob/main/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Tool for scanning Ruby/HAML/Vue/JS codebases and track their Pajamas (GitLab Design System) Adoption

## Motivation

Ideally the tool helps us (more or less in order of priority):

1. identify where in the codebase we use outdated components
2. create and maintain issues and epics for migration efforts
3. provide dashboards for developers and stake-holders to see Pajamas adoption
4. show historical progress in Pajamas adoption (so, identifying up-to-date components as well)
5. show warnings in developers' merge requests if they introduce outdated components, e.g. through Danger or Code Quality

This is all very much in progress. We have chosen to experiment with [Semgrep],
as it seems very powerful and it is already used for SAST-Scanners at GitLab.

You can find the supported components in the [rules/components](./rules/components/) folder.

For now we chose to have this as a separate project in order to iterate faster _and_ potentially
reuse the rules in other projects and/or make it easier to run the scans on older versions of GitLab.

## Prerequisites

1. Install [Semgrep].

## Usage

Scan GitLab
You can use Semgrep with the latest rules, which are published via GitLab pages.

```sh
# inside the GitLab folder
semgrep -c https://gitlab-org.gitlab.io/frontend/pajamas-adoption-scanner/pas-rules.yml  {ee/,}app
```

### Create issues for all warnings of a component

Combine Semgrep with [jq](https://stedolan.github.io/jq/) and the [Batch Issue Creator](https://gitlab.com/gitlab-org/frontend/playground/batch-issue-creator) to create an issue for each warning.

**Example:**

This example looks for all warnings for the card component in all Haml files and returns only the filename, one per line.

```bash
semgrep -c <path to this repo>/rules/components/card <path to GitLab repo>/{ee/,}app/**/*.haml --severity WARNING --json | jq -r '[.results[] | .path] | unique | .[]'
```

## Developing locally

Follow the [prerequisites](#prerequisites), then:

1. Clone this repository
2. Symlink an existing local copy of the GitLab repository to `gitlab`
    ```sh
    ln -s <path_to_gitlab> gitlab
    ```

### Dashboard

The dashboard needs to have scan data available to work. To create that data,
run:

```sh
bin/scan_gitlab_json.sh HEAD
cp tmp/current.json dashboard/static
```

This will run a scan against the currently checked-out HEAD of the local GitLab
repository you symlinked (or cloned) in [prerequisites](#prerequisites). Any
valid ref can be used instead of HEAD, but be aware that the script will try to
check that ref out in order to run the scan.

Then, you can run the development server:

```sh
cd dashboard
yarn install
yarn dev
```

### Run tests

```sh
bin/test.sh
```

## Author

👥 **[GitLab Foundations team](https://about.gitlab.com/handbook/engineering/development/dev/ecosystem/foundations/)**

## Contributing

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/gitlab-org/frontend/pajamas-adoption-scanner/-/issues).

- If you want to contribute new rules, have a look at the readme [rules](./rules)

[Semgrep]: https://semgrep.dev
