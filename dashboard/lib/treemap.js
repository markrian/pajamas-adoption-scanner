import defaults from 'lodash/defaults';

const isLeafNode = (node) => typeof node.value === 'number';

/**
 * Removes parent nodes which only contain other parent nodes.
 * @param {Array} nodes An array of treemap nodes.
 * @returns {Array} An array of treemap nodes.
 */
const flattenRedundantParents = (nodes) =>
  nodes.flatMap((node) => {
    if (isLeafNode(node)) return node;

    if (node.children.length === 0) {
      throw new Error('Group found with no children - this should be impossible!');
    }

    if (node.children.every((child) => !isLeafNode(child))) {
      return flattenRedundantParents(node.children);
    }

    return node;
  });

/**
 * Returns the maximum number of levels in an array of nodes.
 *
 * For example, in pseudocode:
 *   treeLevels([]) === 1
 *   treeLevels([leaf, leaf]) === 2
 *   treeLevels([parent([leaf, leaf]), leaf]) === 3
 *
 * @param {Array} nodes An array of tree nodes.
 * @returns {number} The tree's levels.
 */
const treeLevels = (nodes) =>
  1 + Math.max(0, ...nodes.map((node) => (isLeafNode(node) ? 1 : treeLevels(node.children))));

const getLevelsConfig = (numLevels) => {
  const validLevels = [2, 3];
  if (!validLevels.includes(numLevels)) {
    throw new Error(`Number of levels must be one of ${validLevels.join(', ')}; got ${numLevels}`);
  }

  const topLevel = {
    itemStyle: {
      gapWidth: numLevels === 2 ? 1 : 5,
    },
    upperLabel: {
      show: true,
      formatter: '{c} total findings',
    },
  };

  const componentLevel = {
    itemStyle: {
      borderColor: '#eee',
      borderWidth: 2,
      gapWidth: 1,
    },
    upperLabel: {
      show: true,
      formatter: '{b}: {c} findings',
    },
    emphasis: {
      itemStyle: {
        borderColor: '#aaa',
      },
    },
  };

  const ruleLevel = {
    itemStyle: {
      gapWidth: 1,
    },
    label: {
      formatter: '{b}\n{c} findings',
    },
  };

  return numLevels === 2 ? [topLevel, ruleLevel] : [topLevel, componentLevel, ruleLevel];
};

const scanToTreeMapSeriesNoGrouping = (scan) => {
  const ruleFindingCountMap = scan.results.reduce((acc, result) => {
    const [ruleName] = result.check_id.split('.').reverse();

    if (acc.has(ruleName)) {
      acc.set(ruleName, acc.get(ruleName) + 1);
    } else {
      acc.set(ruleName, 1);
    }

    return acc;
  }, new Map());

  const treeNodes = Array.from(ruleFindingCountMap.entries()).map(([ruleName, count]) => ({
    name: ruleName,
    value: count,
  }));

  return {
    name: 'rules',
    levels: getLevelsConfig(treeLevels(treeNodes)),
    data: treeNodes,
  };
};

const scanToTreeMapSeriesWithGrouping = (scan) => {
  const ruleFindingCountMap = scan.results.reduce((acc, result) => {
    const ruleHierarchy = result.check_id.split('.');

    let previousNode;
    ruleHierarchy.forEach((name, index) => {
      let node;

      if (index === 0) {
        // Root parent

        // Create a top-level node if necessary
        if (!acc.has(name)) {
          node = { name, children: [] };
          acc.set(name, node);
        } else {
          node = acc.get(name);
        }
      } else if (index === ruleHierarchy.length - 1) {
        // Leaf (i.e., rule)

        node = previousNode.children.find((child) => child.name === name);

        if (node) {
          node.value += 1;
        } else {
          node = { name, value: 1 };
          previousNode.children.push(node);
        }
      } else {
        // Some intermediate parent

        // This parent may have already been created by another rule
        node = previousNode.children.find((child) => child.name === name);

        if (!node) {
          // This is the first time we're seeing this parent, so create it
          node = { name, children: [] };
          previousNode.children.push(node);
        }
      }

      // Make this parent's node available to next iteration
      previousNode = node;
    });

    return acc;
  }, new Map());

  // Remove unnecessary intermediate groups
  const treeNodes = flattenRedundantParents(Array.from(ruleFindingCountMap.values()));

  return {
    name: 'components',
    levels: getLevelsConfig(treeLevels(treeNodes)),
    data: treeNodes,
  };
};

const defaultOptions = {
  type: 'treemap',
  width: '100%',
  height: '100%',
  animation: false,
};

/**
 * Transforms the JSON output of a semgrep scan into a series object for
 * charting.
 * @param {Object} The JSON output of a semgrep scan.
 * @returns {Object} A series object consumable by eCharts.
 */
export const scanToTreeMapSeries = (scan, { group = false } = {}) => {
  const series = group
    ? scanToTreeMapSeriesWithGrouping(scan)
    : scanToTreeMapSeriesNoGrouping(scan);

  return defaults(series, defaultOptions);
};
