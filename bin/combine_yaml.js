#!/usr/bin/env node

import { parse, stringify } from "yaml";

import { globby } from "globby";

import { fileURLToPath } from "node:url";
import { join, dirname, sep } from "node:path";
import { readFile, writeFile, mkdir } from "node:fs/promises";

const __dirname = fileURLToPath(new URL(".", import.meta.url));

/**
 * Loads and returns rules from a file and extends the ID with a prefix (pas) and a directory path of the file with rules,
 *  e.g. a rule with the id `baz-schmatz` in a file rules/foo/bar.yml will have the following ID:
 * `pas.rules.foo.baz-schmatz`
 *
 * @param filePath {String} path to the file with semgrep rules
 * @returns {Promise<Array>} Array of semgrep rules
 */
async function loadRules(filePath) {
  const { rules } = parse(
    await readFile(join(__dirname, "..", filePath), "utf-8")
  );

  return rules.map(({ id, ...rule }) => {
    return {
      id: ["pas", ...dirname(filePath).split(sep), id].join("."),
      ...rule,
    };
  });
}

const paths = await globby(["rules/**/*.yml"], { cwd: join(__dirname, "..") });

const rules = (await Promise.all(paths.map(loadRules))).flat();

const targetFile = join(__dirname, "..", "tmp/pas-rules.yml");

await mkdir(dirname(targetFile), { recursive: true });

await writeFile(targetFile, stringify({ rules }), "utf-8");
