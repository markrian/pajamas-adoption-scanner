#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

REF="${1:-HEAD}"

SEMGREP_RESULTS_JSON="$ROOT_DIR/tmp/current.json"

# Scan REF, write JSON results
"$ROOT_DIR"/bin/scan_gitlab.sh \
  "$REF" \
  --json \
  --output "$SEMGREP_RESULTS_JSON"
