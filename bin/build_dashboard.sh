#!/bin/sh
set -eu

# 1. Make scan results available to dashboard build
cp tmp/current.json dashboard/static

# 2. Build dashboard
rm -rf tmp/dashboard

cd dashboard
yarn install --frozen-lockfile
yarn generate
cd ..

mv dashboard/dist tmp/dashboard
