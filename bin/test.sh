#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export SEMGREP_SEND_METRICS=off

semgrep --version
semgrep --test --config rules/ rules/
