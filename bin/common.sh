# This file defines shared options, variables and functions for scripts. It is
# not meant to be called directly (hence it is not executable).
#
# Ignore unused variables; they are used in scripts that source this one.
# shellcheck disable=SC2034
# shellcheck shell=bash

set -euo pipefail
IFS=$'\n\t'

ROOT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")/.." &>/dev/null && pwd)

CI="${CI:-}"

REPO=gitlab
MAIN_BRANCH=master

mkdir -p "$ROOT_DIR/tmp"

warn() {
  printf >&2 '%s\n' "$*"
}

fail() {
  warn "$*"
  exit 1
}
