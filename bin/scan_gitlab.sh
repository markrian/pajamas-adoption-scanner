#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

export SEMGREP_SEND_METRICS=off

ORIGINAL_REF=
REF="${1:-HEAD}"

cd "$ROOT_DIR"

print_help() {
  cat <<HELP_MESSAGE
Usage: $(basename "$0") [REF [SEMGREP_OPTIONS...]]
Scans the GitLab repository at commit REF.

If given no arguments, scans HEAD, which is useful for local scans. Arguments \
after REF are passed to semgrep. This means that REF is required if passing \
arguments to semgrep.

In CI, the GitLab repository is cloned at REF.

To run this locally, first symlink the GitLab repository to "$REPO". \
For example:

    ln -s <path_to_gitlab_repo> $REPO

OPTIONS
  -h, --help     print this help message

WARNING
This script mutates the GitLab repository in the following ways:
  - Updates the origin/$MAIN_BRANCH remote branch
  - Checks out REF (and attempts to check out the original ref before exiting)
HELP_MESSAGE
}

attempt_to_restore_original_git_checkout() {
  [ -z "$ORIGINAL_REF" ] && return 0

  cd "$ROOT_DIR/$REPO"
  warn "Restoring repo checkout to $ORIGINAL_REF..."
  git checkout "$ORIGINAL_REF" || true
}

ensure_gitlab_checkout() {
  if [ -n "$CI" ]; then
    warn "Cloning gitlab@${REF} into gitlab/"
    git clone https://gitlab.com/gitlab-org/gitlab.git \
      --depth=1 \
      --branch "$REF" \
      "$REPO"
    cd "$REPO"
  else
    warn "Not running in CI, so assuming gitlab dir/link already exists..."

    cd "$REPO" || fail "Have you run \`ln -s <path_to_gitlab> gitlab\`?"

    warn "Updating to $REF..."
    ORIGINAL_REF="$(git rev-parse --symbolic --abbrev-ref @)"
    git fetch origin "$MAIN_BRANCH"
    git checkout "$REF"
  fi
}

for arg in "$@"; do
  if [ "$arg" = "-h" ] || [ "$arg" = "--help" ]; then
    print_help
    exit
  fi
done

if [ "$#" -gt 0 ]; then
  # Drop REF argument so we can pass remaining arguments to semgrep
  shift 1
fi

# Before this script terminates, attempt to restore the GitLab repository to
# the commit/ref it was checked out to before the script was run.
# Has no effect in CI.
trap attempt_to_restore_original_git_checkout EXIT

ensure_gitlab_checkout

semgrep --config "$ROOT_DIR/rules" {,ee/}app "$@"
